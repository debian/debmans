Contributing
============

The ``debmans`` project welcomes contributions of all sort.

A broader discussion about the service is in the `manpages.debian.org
wiki page <https://wiki.debian.org/manpages.debian.org>`__.

Source code
-----------

The source code should be available in this `Gitlab
repository <https://gitlab.com/anarcat/debmans/>`__. Gitlab was chosen
because this project hopes to be reused by other Debian derivatives and
we prefer to have a neutral space to develop the software. Gitlab also
provides an `issue queue <https://gitlab.com/anarcat/debmans/issues>`__
to `report a new
issue <https://gitlab.com/anarcat/debmans/issues/new>`__.

Debian developers not desiring to use a semi-proprietary platform like
Gitlab may still use the `collab-maint
repository <https://anonscm.debian.org/git/collab-maint/debmans.git/>`__,
see
`CollaborativeMaintenance <https://wiki.debian.org/CollaborativeMaintenance>`__
for more information on how to collaborate on those repositories. The
two repositories are synchronized every hour.

Issues, feature requests and security
-------------------------------------

Patches can be sent by email (see below) or with Gitlab `Merge
Requests <https://gitlab.com/anarcat/debmans/merge_requests/new>`__.

Security issues can be privately reported on Gitlab or directly to the
author, at anarcat@debian.org. You can use this OpenPGP public key to
encrypt messages for confidential security issues::

    8DC9 01CE 6414 6C04 8AD5  0FBB 7921 5252 7B75 921E

Test suite
----------

Make sure you run tests before you send a patch. Also add tests for new
functionalities you add or for bugs you find. Tests are done with
`pytest <http://pytest.org/>`__::

    py.test

Test coverage can be verified with::

    py.test --cov debmans

Exact lines lacking test coverage are available in the HTML report that
can be generated with::

    py.test --cov debmans --cov-report html

The test suite requires the ``dget`` and ``dpkg-scansources`` commands
from the `devscripts package
<https://tracker.debian.org/pkg/devscripts>`__ to build a test
mirror. Network access is required if packages are not locally cached
in ``/var/cache/apt/archives`` or where ``dget`` can find them (see
``DGET_PATH`` in the ``dget(1)`` manpage).

Release process
---------------

1. make sure tests pass (see above for details)::

     py.test

2. create a signed and annotated tag::

     git tag -s x.y.z

3. build and test Python "wheel"::

     python setup.py bdist_wheel
     sudo pip install dist/*.whl
     debmans --version
     debmans -m /var/cache/apt/archives -o html -v --progress extract render site serve
     sudo pip uninstall debmans

4. push commits and tags to the git repository::

     git push
     git push --tags

5. publish Python "wheel" on PyPI::

     twine upload dist/*

6. announce on debian-doc@lists.debian.org
