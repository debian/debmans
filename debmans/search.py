# coding: utf-8
'''search engine for debmans'''
# Copyright (C) 2016 Antoine Beaupré <anarcat@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import division, absolute_import
from __future__ import print_function

import logging
logger = logging.getLogger(__name__)
import os
import os.path
import re

import click
from flask import Flask, render_template, request, redirect

from debmans.renderer import find_files, ManpageRenderer

app = Flask(__name__, static_url_path='')


@app.route('/search')
def search():
    '''respond to searches requests

    .. todo:: unit test this: http://flask.pocoo.org/docs/0.11/testing/

    .. note:: inspired by http://manpages.ubuntu.com/cgi-bin/search.py?q=man
    '''
    # XXX: not sure this is the best structure
    manpages = {}  # section -> (name, href)
    i = 0
    assert app.static_folder
    pattern = request.values.get('pattern', None)
    locale = request.values.get('locale', None)
    section = request.values.get('section', None)
    suites = app.config['releases']
    default_suite = 'sid'
    if 'sid' not in suites:
        # arbitrarily: last key in the list
        default_suite = sorted(suites.keys())[-1]
    suite = request.values.get('suite', default_suite)
    logger.debug('using suite %s' % suite)
    if pattern is None:
        return render_template('template.html',
                               suites=app.config['releases'],
                               content='missing "pattern" parameter')
    logger.debug('searching for pattern: %s', pattern)
    filelist = find_files(app.static_folder, app.config['patterns'])
    for module, path, match in filelist:
        name = match.group('name')
        if (pattern not in name
            or locale != match.group('locale')
            or (suite != match.group('suite'))
            or (section is not None and
                section != match.group('section'))):
            continue
        i += 1
        if match.group('section') not in manpages:
            manpages[match.group('section')] = set()
        logger.info('found manpage %s(%s) in %s %s', name,
                    match.group('section'),
                    match.group('suite'),
                    match.group('locale'))
        href = match.group()
        base, ext = os.path.splitext(href)
        href = base + '.html'
        manpages[match.group('section')].add((name,
                                              href))
    if i == 1:
        return redirect(href)
    # XXX: this should also know about all sections, probably?
    return render_template('search.html', manpages=manpages, count=i,
                           suites=suites, suite=suite)


@app.route('/')
def root():
    '''serve the default HTML file

    this route should never be hit in a production environment'''
    return app.send_static_file('index.html')


@app.route('/<path:path>')
def static_files(path):
    '''serve static files in development

    this roule should never be hit in a production environment'''
    return app.send_from_directory(app.static_folder, path)


@click.command()
@click.option('-p', '--port', default=8000, show_default=True,
              help='port to start the server on')
@click.pass_obj
def serve(obj, port):
    '''serve deployed files in a basic web browser

    also supports basic searching

    this is used for development purposes and is not designed to be a
    fully-fledged webserver
    '''
    main(obj['output'], port, releases=obj['mirror'].releases,
         patterns=obj['patterns'])


def main(output, port=8000, releases={}, patterns={}):
    app.static_folder = os.path.abspath(output)
    app.config['releases'] = releases
    app.config['patterns'] = patterns
    app.run(port=port)


if __name__ == '__main__':
    main(output='.')
