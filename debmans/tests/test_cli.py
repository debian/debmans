#!/usr/bin/python3
# coding: utf-8

# Copyright (C) 2016 Antoine Beaupré <anarcat@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os.path
import re
import traceback

from click.testing import CliRunner

from debmans.__main__ import debmans
from debmans.tests.test_extractor import fake_repo


from debmans.logger import setup_logging

setup_logging(level='DEBUG')


def test_usage():
    runner = CliRunner()
    result = runner.invoke(debmans, ['--help'])
    assert result.exit_code == 0


def test_extract_render_chain(fake_repo, tmpdir):
    repo = fake_repo
    runner = CliRunner()
    output = tmpdir.join('html')
    result = runner.invoke(debmans, ['--output', str(output),
                                     '--mirror', str(repo),
                                     'extract', 'render'], obj={})
    if result.exit_code != 0:
        bt = "".join(traceback.format_exception(*result.exc_info))
        print('backtrace: %s' % bt)
        print("output %s" % result.output)
    assert result.exit_code == 0
    assert output.check()
    # descend in suite
    files = list(output.visit())
    bases = [os.path.basename(str(x)) for x in files]
    assert 'man.1.gz' in bases, "manpage not extracted"
    assert 'man.1.html' in bases, "html not generated"
    prefixes = [re.sub(r'/man/man1/.*$', '', str(x))
                for x in files if '/man/man1/' in str(x)]
    assert len(set(prefixes)) == 1, "not in the same directory"
    tmpdir.chdir()
    # this should be the same
    result = runner.invoke(debmans, ['--output', 'html',
                                     '--mirror', str(repo),
                                     '--no-cache',
                                     'extract', 'render'], obj={})
    assert result.exit_code == 0
    assert '0 paths from extractor' not in result.output
    files = list(output.visit())
    prefixes = [re.sub(r'/man/man1/.*$', '', str(x))
                for x in files if '/man/man1/' in str(x)]
    assert len(set(prefixes)) == 1, "not in the same directory"
