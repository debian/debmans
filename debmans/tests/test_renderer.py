#!/usr/bin/python3
# coding: utf-8

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import os.path

import pytest

from debmans.extractor import PackageMirror
from debmans.renderer import JinjaRenderer, CommandRenderer, MarkdownRenderer, CommandRendererError, DefaultManpageRenderer, find_files
from debmans.tests.test_extractor import test_extractor, fake_repo, MANPAGES_COUNT
from debmans.utils import find_static_file


def test_jinja(tmpdir):
    output = tmpdir.join('output.html')
    template = find_static_file(os.path.join('templates', 'template.html'))
    r = JinjaRenderer(template=template)
    r.render(str(output), content="foo\nbar", suites={})
    data = output.read().decode('utf-8')
    assert 'foo' in data, 'missing content'
    assert '\\' not in data, 'escaped when it should not'
    assert 'Generated' in data, 'missing pageinfo'
    mtime = os.stat(str(output)).st_mtime
    r.render(str(output), content="foo\nbar", suites={})
    assert mtime == os.stat(str(output)).st_mtime, 'cache not working'
    r = JinjaRenderer(template=template, cache=False)
    r.render(str(output), content="foo\nbar", suites={})
    assert mtime != os.stat(str(output)).st_mtime, 'cache toggle not working'
    

def test_command_render(tmpdir):
    output = tmpdir.join('output.html')
    template = find_static_file(os.path.join('templates', 'template.html'))
    r = CommandRenderer(command='cat "%(source)s"', template=template)
    r.render(__file__, str(output), suites={})
    data = output.read().decode('utf-8')
    assert 'test_command_render' in data, 'content missing'


def test_static_render(tmpdir):
    source = tmpdir.join('source.mdwn')
    source.write('# Foo')
    output = tmpdir.join('output.html')
    template = find_static_file(os.path.join('templates', 'template.html'))
    r = MarkdownRenderer(template)
    r.render(str(source), str(output), suites={})
    data = output.read().decode('utf-8')
    assert '<H1>' in data.upper(), 'heading rendering failed'


def test_manpage_rendering(fake_repo, tmpdir):
    manpages, _ = test_extractor(fake_repo, tmpdir)
    template = find_static_file(os.path.join('templates', 'template.html'))
    suites = PackageMirror(str(fake_repo)).releases
    print("releases: %s" % suites)
    for manpage in manpages:
        r = DefaultManpageRenderer(template)
        base, _ = os.path.splitext(os.path.basename(manpage))
        output = tmpdir.join(base + '.html')
        r.render(manpage, str(output), suites=suites)
        html = output.read().decode('utf-8')
        assert '<h2>NAME</h2>' in html, "doesn't look like a manpage"
        assert '8.6 jessie (stable)' in html
        assert '<title></title>' not in html, 'title should not be empty'


def test_manpage_render_only(fake_repo, tmpdir):
    manpages, _ = test_extractor(fake_repo, tmpdir)
    patterns = {}
    patterns[DefaultManpageRenderer.pattern] = DefaultManpageRenderer
    files = list(find_files(str(tmpdir), patterns))
    assert len(files) == MANPAGES_COUNT, '''found the wrong number of files'''


def test_manpage_norender(tmpdir):
    template = find_static_file(os.path.join('templates', 'template.html'))
    r = DefaultManpageRenderer(template)
    output = tmpdir.join('nosuchpage.1.html')
    with pytest.raises(CommandRendererError):
        r.render('nosuchpage.1.gz', str(output), suites={})
    assert not output.exists(), 'a missing manpage should not create HTML file'
