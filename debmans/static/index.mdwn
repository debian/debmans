Welcome to the Debian Manpage Repository
----------------------------------------

This site contains hundreds of thousands of dynamically generated
manuals, extracted from every package of every supported version of
Debian, and updated on a daily basis. Traditionally, manpages are
browsed on the command line. This project renders all such manuals
included in Debian into an HTML, web-browsable format.

Enter the manual page you are looking for in the search box to
continue.

Manual sections
---------------

* [intro(1)](/search?pattern=intro&section=1) - user commands
* [intro(2)](/search?pattern=intro&section=2) - system calls
* [intro(3)](/search?pattern=intro&section=3) - library functions
* [intro(4)](/search?pattern=intro&section=4) - special files
* [intro(5)](/search?pattern=intro&section=5) - file formats
* [intro(6)](/search?pattern=intro&section=6) - games
* [intro(7)](/search?pattern=intro&section=7) - overview, conventions, and miscellany section
* [intro(8)](/search?pattern=intro&section=8) - administration and privileged commands

Other resources
---------------

* [Debian.org documentation portal](https://www.debian.org/doc/)
* [Debian Wiki](https://wiki.debian.org/) and [other Debian resources](https://wiki.debian.org/DebianResources)
