License
=======

.. include:: ../LICENSE.rst

.. include:: ../README.rst
   :start-after: .. split-acknowledgements
