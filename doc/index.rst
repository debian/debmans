.. debmans documentation master file, created by
   sphinx-quickstart on Fri Nov 18 16:37:08 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../README.rst
   :end-before: .. split

Contents:

.. toctree::
   :maxdepth: 2

   usage
   design
   contributing
   api
   todo
   license

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

